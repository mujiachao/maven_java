package com.mu.beantomap;

import java.util.List;

/**
 * @author MJC
 * @date 2020-2-27 14:12
 * @description
 */
public class User {
    private String userId;

    private String userName;

    private List<Role> roleList;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public User() {}

    public User(String userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }
}

