package com.mu.beantomap;

/**
 * @author MJC
 * @date 2020-2-27 14:15
 * @description
 */
public class Role {
    private String userId;

    private String roleName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Role() {
    }

    public Role(String userId, String roleName) {
        this.userId = userId;
        this.roleName = roleName;
    }
}
