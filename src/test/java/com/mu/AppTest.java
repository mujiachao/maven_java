package com.mu;

import static org.junit.Assert.assertTrue;

import com.alibaba.fastjson.JSON;
import com.mu.beantomap.*;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Map;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @Test
    public void shouldAnswerWithTrue() throws Exception {
        //第一种方法测试
        User user = new User();
        user.setUserName("张三");
        user.setUserId("01");

        ArrayList<Role> list = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Role role = new Role();
            role.setRoleName("role"+i+"号");
            role.setUserId(i+"");
            list.add(role);
        }
        user.setRoleList(list);
        Map map = BeanMapUtilByJson.beanToMap(user);
        System.out.println(JSON.toJSONString(map));
        //第二种方法测试
        Map map1 = BeanMapUtilByReflect.beanToMap(user);
        System.out.println(JSON.toJSONString(map1));
        //第三种方法 内省机制
        Map map2 = BeanMapUtilByIntros.beanToMap(user);
        System.out.println(JSON.toJSONString(map2));
        //第四种方法 利用 apache 中的 BeanUtils 进行转换
        Map map3 = BeanMapUtilByApache.beanToMap(user);
        System.out.println(JSON.toJSONString(map3));
        //第五种方法 利用 cglib 中的 BeanMap 进行转换
        Map map4 = BeanMapUtilByCglib.beanToMap(user);
        System.out.println(JSON.toJSONString(map4));
    }


}
