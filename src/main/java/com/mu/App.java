package com.mu;

import java.util.Arrays;
import java.util.Random;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main(String[] args) {
        long before = System.currentTimeMillis();
        int[] arr=getRandomArray();
         for(int i=0;i<arr.length-1;i++){
             int minIndex=i;
             for (int j=i+1;j<arr.length;j++) {
                 if (arr[j]<arr[minIndex]){
                     minIndex=j;
                 }
             }
             int temp=arr[i];
             arr[i]=arr[minIndex];
             arr[minIndex]=temp;
         }
        System.out.println(Arrays.toString(arr));
        long after = System.currentTimeMillis();
        System.out.println(after-before);
        maoPao(getRandomArray());
    }

    public static int[] getRandomArray(){
        int num=10000;
        Random random = new Random();
        int[] arr=new int[10000];
        for (int i = 0; i <num ; i++) {
            arr[i]=random.nextInt(10000);
        }
        return arr;
    }

    public static void maoPao(int[] arr){
        long before = System.currentTimeMillis();
        int[] newArr=arr;
        for (int i = 0; i <arr.length ; i++) {
            for (int j = 0; j <arr.length-i-1 ; j++) {
                if (arr[j]>arr[j+1]){
                    int temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
        long after = System.currentTimeMillis();
        System.out.println(after-before);
    }
}
