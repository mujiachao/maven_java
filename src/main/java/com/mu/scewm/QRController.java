package com.mu.scewm;

/**
 * @author MJC
 * @date 2020-3-6 15:06
 * @description
 */

public class QRController {
    public static void main(String[] args) {
        String text = "二姨大祝：张运泽茁壮成长！";    //生成二维码的内容
        String logoPath = "F:\\img\\afb01e3b79573398baaeeee135d1b81.jpg";    //二维码的logo图片
        String destPath = "F:\\img";                    //生成的二维码存储本地的地址

        try {
            System.out.println(QRCodeUtils.encode(text, logoPath, destPath, true));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
